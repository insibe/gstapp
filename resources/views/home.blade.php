@extends('layouts.guest-dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">GST Laws </h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $getGstLaws->count() }} GST Laws.</p>
                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="row g-gs">
            @if(count($getGstLaws ) > 0)
                @foreach($getGstLaws as $getGstLaw)
            <div class="col-sm-6 col-lg-4 col-xxl-3">
                <div class="card card-bordered h-100">
                    <div class="card-inner">
                        <div class="project">
                            <div class="project-head">
                                <a href="{{ url('gst/'.$getGstLaw->slug ) }}" class="project-title">
                                    <div class="project-info">
                                        <h6 class="title">{{ $getGstLaw->title }}</h6>
                                        <span class="sub-text">{{ $getGstLaw->title }}</span>
                                        {{ $getGstLaw->status }}
                                    </div>
                                </a>

                            </div>
                            <div class="project-details">
                                <p>{{ $getGstLaw->description }}</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
                @endforeach
            @else

            @endif

        </div>
    </div><!-- .
@endsection
