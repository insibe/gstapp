@extends('layouts.dashboard')

@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em
                                class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">


                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-6">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($feed, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                            <div class="form-group">
                                <label class="form-label" >Feed Name <span>*</span></label>
                                <div class="form-control-wrap">
                                    {!! Form::text('title',null, ['class' => 'form-control', 'placeholder'=>'Enter Feed Title..','required' =>'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="phone-no">Feed Description</label>
                                <div class="form-control-wrap">
                                    {!! Form::textarea('description',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter Feed Description..','required' =>'required']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="form-label" >Feed URL  <span>*</span></label>
                                <div class="form-control-wrap">
                                    {!! Form::url('source_url',null, ['class' => 'form-control', 'placeholder'=>'Enter Feed Source URL..','required' =>'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Feed Source Category <span>*</span> </label>
                                <div class="form-control-wrap">
                                    {!! Form::select('type',[''=>'','rss'=>'RSS Feed','wapi'=>'Wordpress REST API'] ,null, ['data-parsley-errors-container' => '#source_category-errors','data-placeholder' => 'Select Feed Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}

                                    <div id="source_category-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Source Language <span>*</span></label>
                                <div class="form-control-wrap">

                                    {!! Form::select('source_lang', [ ''=>'','en'=>'English', 'ml'=>'Malayalam','tn'=>'Tamil','kn'=>'Kannada'] ,null, ['data-parsley-errors-container' => '#source_lang-errors','data-placeholder' => 'Select Source Language','class' => 'form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                    <div id="source_lang-errors"></div>
                                </div>
                            </div>
                                <div class="form-group">
                                    <label class="form-label">Target Language <span>*</span></label>
                                    <div class="form-control-wrap">

                                        {!! Form::select('target_lang', [''=>'','en'=>'English', 'ml'=>'Malayalam','tn'=>'Tamil','kn'=>'Kannada'] ,null, ['data-parsley-errors-container' => '#target_lang-errors','data-placeholder' => 'Select Target Language','class' => ' form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                        <div id="target_lang-errors"></div>
                                    </div>
                                </div>

                            <div class="form-group">
                                <div class="form-label-group">
                                    <label class="form-label">Update Interval <span>*</span></label>
                                </div>

                              <div class="form-control-group">
                                  {!! Form::select('interval', [  ''=>'','30'=>'30 Minutes', '60'=>'01 Hours', '120'=>'02 Hours', '360'=>'06 Hours', '720'=>'12 Hours'] ,null, ['data-parsley-errors-container' => '#interval-errors','data-placeholder' => 'Select Fetch Interval','class' => ' form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                  <div id="interval-errors"></div>
                              </div>

                            </div>

                            <div class="form-group">
                                <label class="form-label" >Feed Source Website </label>
                                <div class="form-control-wrap">
                                    {!! Form::url('website',null, ['class' => 'form-control', 'placeholder'=>'Enter Feed Source URL..','required' =>'required']) !!}
                                </div>
                            </div>
                        <div class="form-group">
                            <label class="form-label" >Feed Logo</label>
                            <div class="form-control-wrap">
                                {!! Form::file('logo',null, ['class' => 'custom-file-input']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" >Feed Status <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('status',[''=>'','0'=>'Active','1'=>'Inactive'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Feed Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>

                            <div class="form-group">
{{--                                <form action="{{url('dashboard/feeds/'.$feed->id)}}" method="post">--}}
{{--                                    @method('DELETE')--}}
{{--                                    @csrf--}}
{{--                                    <button type="submit" class="btn btn-lg btn-danger">Delete</button>--}}
{{--                                </form>--}}

                                <button type="submit" class="btn btn-lg btn-primary float-right">Save Feed</button>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->





@endsection