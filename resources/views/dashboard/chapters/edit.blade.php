@extends('layouts.dashboard')

@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-8">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($chapter, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label class="form-label" >Title <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('title',null, ['class' => 'form-control ', 'placeholder'=>'Enter Title','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Sub Title <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('subtitle',null, ['class' => 'form-control ', 'placeholder'=>'Enter Sub Title','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Select Act<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('act_id',['0' => 'None'] + $acts ,null, ['data-parsley-errors-container' => '#status-acts','data-placeholder' => 'Select Act Law','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                            </div>
                            <div id="status-acts`"></div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="phone-no">Order<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::number('order',null, ['class' => 'form-control ', 'placeholder'=>'Order','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" > Status <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('status',[''=>'','0'=>'Active','1'=>'Inactive'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>

                        <div class="form-group">

                            <button type="submit" class=" float-right btn btn-lg btn-primary">Save</button>
                        </div>

                        {!! Form::close() !!}
                        @if ($mode  === 'EDIT')

                            <form action="{{url('dashboard/categories/'.$chapter->id.'/')}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure?')"   class="btn btn-link" type="submit">Delete Category</button>
                            </form>
                        @else

                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->


@endsection
