@extends('layouts.dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $sections->count() }} Sections.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/sections/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Create New Section</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
{{--    <div class="nk-block nk-block-lg">--}}
{{--        <div class="nk-block-head">--}}
{{--            <div class="nk-block-head-content">--}}
{{--                <h4 class="nk-block-title">Data Table with Export</h4>--}}
{{--                <div class="nk-block-des">--}}
{{--                    <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="card card-preview">--}}
{{--            <div class="card-inner">--}}
{{--                <table class="datatable-init-export nowrap table" data-export-title="Export">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th>Name</th>--}}
{{--                        <th>Position</th>--}}
{{--                        <th>Office</th>--}}
{{--                        <th>Age</th>--}}
{{--                        <th>Start date</th>--}}
{{--                        <th>Salary</th>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                    @if(count($sections ) > 0)--}}
{{--                        @foreach($sections as $section)--}}
{{--                    <tr>--}}
{{--                        <td>   <a href="{{ url('/dashboard/sections/'.$section->id.'/') }}">--}}
{{--                                <span class="tb-lead">   {{ $section->section_no }}  </span>--}}
{{--                            </a></td>--}}
{{--                        <td> <span class="tb-lead">  {{ \Str::limit( $section->chapters->title ?? 'none' ,30) }}  </span></td>--}}
{{--                        <td> <span class="tb-lead">   {{ $section->chapters->acts->title ?? 'none' }}   </span></td>--}}
{{--                        <td> {{ $section->section_heading }}</td>--}}
{{--                        <td>   <a href="{{ url('/dashboard/sections/'.$section->id.'/edit') }}" class="btn btn-xs btn-outline-primary">Edit Section</a></td>--}}
{{--                        <td>$320,800</td>--}}
{{--                    </tr>--}}
{{--                        @endforeach--}}
{{--                    @else--}}

{{--                    @endif--}}
{{--                    </tbody>--}}
{{--                </table>--}}
{{--            </div>--}}
{{--        </div><!-- .card-preview -->--}}
{{--    </div> <!-- nk-block -->--}}
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Title</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Description</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Actions</span></div>
            </div><!-- .nk-tb-item -->


            @if(count($sections ) > 0)
                @foreach($sections as $section)
                    <div class="nk-tb-item">

                        <div class="nk-tb-col">
                            <a href="{{ url('/dashboard/sections/'.$section->id.'/') }}">
                                <span class="tb-lead">   {{ $section->section_no }}  </span>
                            </a>
                        </div>

                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead">  {{ $section->chapters->title ?? 'none'  }}  </span>
                        </div>
                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead">    {{  $section->chapters->acts->title ?? 'none'  }}    </span>
                        </div>
                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead">   {{ $section->chapters->id  }}    </span>
                        </div>

                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead">   {{ $section->order  }}    </span>
                        </div>

                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead">  {{ \Str::limit($section->section_heading ?? 'none' ,30) }}    </span>
                        </div>

                        <div class="nk-tb-col tb-col-lg">
                            <a href="{{ url('/dashboard/sections/'.$section->id.'/edit') }}" class="btn btn-xs btn-outline-primary">Edit Section</a>
                        </div>



                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $sections->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>

@endsection
