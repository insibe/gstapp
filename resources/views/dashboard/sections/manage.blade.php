@extends('layouts.dashboard')

@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    <p>Manage Sub Sections</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
{{--                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>--}}
{{--                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalCreate"><em class="icon ni ni-plus"></em><span>Add Sub Sections</span></a>--}}
                                <a href="{{ url('/dashboard/sections/'.$section->id.'/subsections/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Add Sub Sections</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Sub Section Title</span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">Revision </span></div>
                <div class="nk-tb-col tb-col-mb"><span class="sub-text">With Effect From</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Actions</span></div>
            </div><!-- .nk-tb-item -->


            @if(count($subsections ) > 0)
                @foreach($subsections as $subsection)
                    <div class="nk-tb-item">
                        <div class="nk-tb-col">
                            <a href="{{ url('/dashboard/sections/'.$section->id.'/subsections/'.$subsection->id.'/edit') }}">
                                <span class="tb-lead">   {{ $subsection->title }} </span>
                            </a>
                        </div>

                        <div class="nk-tb-col tb-col-md">
                            <span class="tb-lead"> {{ $subsection->parent_id }} </span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <span class="tb-lead">   {{ $subsection->wef }}  </span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <a href="{{ url('/dashboard/sections/'.$section->id.'/subsections/create?parent='.$subsection->id.'') }}" class="btn btn-xs btn-outline-primary">Add Amendment</a>
                        </div>




                    </div><!-- .nk-tb-item -->

                    @if( $section->id = $subsection->parent_id )
                        <p>sdfdsf</p>
                    @endif


                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">


                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>

    <!-- @@ Category Modal @e -->
    <div class="modal fade" id="modalCreate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal" aria-label="Close"> <em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <h5 class="title">Add Category</h5>
                    <form action="#" class="pt-2">
                        <div class="form-group">
                            <label class="form-label" for="full-name">Category Name</label>
                            <div class="form-control-wrap">
                                <input type="text" class="form-control" id="full-name" placeholder="e.g. Web Development">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="description">Description</label>
                            <div class="form-control-wrap">
                                <textarea class="form-control" name="description" id="description" placeholder="Write Category Description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="thumb">Category thumbnail</label>
                            <div class="form-control-wrap">
                                <div class="custom-file">
                                    <input type="file" multiple class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="thumb">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Subcategory</label>
                            <div class="d-flex gx-3 mb-3">
                                <div class="g w-100">
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" placeholder="e.g. Web Development">
                                    </div>
                                </div>
                                <div class="g">
                                    <button class="btn btn-icon btn-outline-light"><em class="icon ni ni-plus"></em></button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button data-dismiss="modal" type="submit" class="btn btn-primary">Save Informations</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
