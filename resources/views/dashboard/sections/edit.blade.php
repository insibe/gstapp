@extends('layouts.dashboard')
@section('css_before')
    <link rel="stylesheet" href="{{ asset('backend/css/editors/tinymce.css') }}">


@endsection

@section('js_after')
    <script src="{{ asset('backend/js/libs/editors/tinymce.js') }}"></script>
    <script src="{{ asset('backend/js/editors.js') }}"></script>

    <script>


    </script>
@endsection


@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-8">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($section, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                        <div class="form-group">
                            <label class="form-label"> Language <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('locale' ,[''=>'','ml'=>'Malayalam','en'=>'English'],null, ['data-parsley-errors-container' => '#language-errors','data-placeholder' => 'Select Language','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                                <div id="language-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Select Chapter<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('chapter_id',['0' => 'None'] + $chapters ,null, ['data-parsley-errors-container' => '#status-acts','data-placeholder' => 'Select Act Law','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                            </div>
                            <div id="status-acts`"></div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" > Section No <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('section_no',null, ['class' => 'form-control ', 'placeholder'=>'Section No','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="phone-no">Section Heading<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('section_heading',null, ['class' => 'form-control ', 'placeholder'=>'Section Heading','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="phone-no">Section Header</label>
                            <div class="form-control-wrap">

                                {!! Form::textarea('section_header',null, ['class' => 'form-control tinymce-toolbar','rows' => 1,'placeholder'=>'Section Header']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Section Footer</label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('section_footer',null, ['class' => 'form-control tinymce-toolbar','rows' => 1,'placeholder'=>'Section Footer']) !!}
                            </div>
                            <div id="status-parent"></div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" >With Effect From<span>*</span></label>
                            <div class="form-control-wrap">
                                <div class="form-icon form-icon-left">
                                    <em class="icon ni ni-calendar"></em>
                                </div>
                                {!! Form::text('wef',null, ['class' => 'form-control date-picker ', 'placeholder'=>'With Effect From','required' =>'required',' data-date-format'=>'yyyy-mm-dd']) !!}

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" for="phone-no">Order<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::number('order',null, ['class' => 'form-control ', 'placeholder'=>'Order','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Section Status <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('status',[''=>'','0'=>'Active','1'=>'Inactive'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>

                        </div>
                        <div class="form-group">

                            <button  type="submit" class="  float-right btn btn-lg btn-primary">Save</button>
                        </div>

                        {!! Form::close() !!}
                        @if ($mode  === 'edit')
                            <form action="{{url('dashboard/sections/'.$section->id)}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure?')"   class="btn btn-link" type="submit">Delete Section</button>
                            </form>
                        @else

                        @endif
                    </div>

                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->



@endsection
