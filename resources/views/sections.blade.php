@extends('layouts.guest-dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title"> </h3>
                <div class="nk-block-des text-soft">



                </div>

            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">

        <div class="row ">
            <div class="col-lg-12">
                <div class="card card-bordered ">
                    <div class="card-inner">
                        @if(count($acts ) > 0)

                            @foreach($acts as $act)
                              <ul>
                                  <li class="pb-2">
                                      <h4> <a href="{{ url('/acts/'.$act->slug.'/') }}"> {{ $act->title }}</a></h4>
                                      <p class="small">{{ $act->description }}</p>
                                 </li>
                              </ul>
                            @endforeach
                        @else
                        @endif



{{--                        @if(count($sections ) > 0)--}}

{{--                            <ul>--}}
{{--                                @foreach($sections as $section)--}}
{{--                                    <li>--}}
{{--                                        <h6 class="pb-1 pt-1"> {{ $section->section_no }} - {{ $section->section_heading }}</h6>--}}

{{--                                        {!! $section->section_header !!}--}}


{{--                                        <ul>--}}

{{--                                            @foreach($section->subsections as $subsection)--}}

{{--                                                <li class=" pt-2">   {!! $subsection->content_formatted !!}</li>--}}


{{--                                            @endforeach--}}
{{--                                        </ul>--}}
{{--                                        {!! $section->section_footer !!}--}}
{{--                                    </li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        @else--}}

{{--                        @endif--}}
                    </div>
                </div>
            </div>

        </div>
    </div><!-- .
@endsection
