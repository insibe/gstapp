@extends('layouts.inner')

@section('content')

    <section class="section">

        <div class="container">
            <div class="row align-items-center position-relative mt-5" style="z-index: 1;">
                <div class="col-lg-6 col-md-12">
                    <div class="title-heading mt-4 text-center text-lg-start">
                        <span>Introducing gstcentral.</span>
                        <h1 class="heading mb-3 title-dark we_help">All-in-one <br>
                            GST Platform You Need.</h1>
                        <p class="para-desc ">India's most-trusted Cloud Platform For GST Updates,Guides,Tools & Resources in Regional Languages.</p>

                    </div>

                </div><!--end col-->

                <div class="col-lg-6 col-md-12 mt-4 pt-2">
                    <div class="position-relative">
                        <div class="software_hero">
                            <img src="{{ asset('/assets/images/software/software.png') }}" class=" mover mx-auto img-fluid d-block" alt="">
                        </div>
                        <div class="play-icon">
                            <a href="#" data-bs-toggle="modal" data-bs-target="#watchvideomodal" class="play-btn video-play-icon">
                                <i class="mdi mdi-play text-primary rounded-circle bg-white shadow"></i>
                            </a>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div>

        <div class="container mt-4">
            <div class="row justify-content-center">
                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/amazon.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/google.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/lenovo.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/paypal.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/shopify.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                    <img src="images/client/spotify.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section>
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title mb-4 pb-2">
                        <h4 class="title mb-4">GstCentral Features</h4>
                        <p class="text-muted para-desc mb-0 mx-auto">Thoughtful features for an exceptional portal experience.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-lg-3 col-md-4 mt-4 pt-2">
                    <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-chart-line"></i>
                            </span>
                        <div class="card-body p-0 content">
                            <h5>library</h5>
                            <p class="para text-muted mb-0">It is a long established fact that a reader.</p>
                        </div>
                        <span class="big-icon text-center">
                                <i class="uil uil-chart-line"></i>
                            </span>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-4 mt-4 pt-2">
                    <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-crosshairs"></i>
                            </span>
                        <div class="card-body p-0 content">
                            <h5>advanced search </h5>
                            <p class="para text-muted mb-0">It is a long established fact that a reader.</p>
                        </div>
                        <span class="big-icon text-center">
                                <i class="uil uil-crosshairs"></i>
                            </span>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-4 mt-4 pt-2">
                    <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-airplay"></i>
                            </span>
                        <div class="card-body p-0 content">
                            <h5>tools & resources</h5>
                            <p class="para text-muted mb-0">It is a long established fact that a reader.</p>
                        </div>
                        <span class="big-icon text-center">
                                <i class="uil uil-airplay"></i>
                            </span>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-4 mt-4 pt-2">
                    <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-rocket"></i>
                            </span>
                        <div class="card-body p-0 content">
                            <h5>News & Notifications</h5>
                            <p class="para text-muted mb-0">It is a long established fact that a reader.</p>
                        </div>
                        <span class="big-icon text-center">
                                <i class="uil uil-rocket"></i>
                            </span>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-4 mt-4 pt-2">
                    <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-clock"></i>
                            </span>
                        <div class="card-body p-0 content">
                            <h5>briefings & analysis</h5>
                            <p class="para text-muted mb-0">It is a long established fact that a reader.</p>
                        </div>
                        <span class="big-icon text-center">
                                <i class="uil uil-clock"></i>
                            </span>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-4 mt-4 pt-2">
                    <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-users-alt"></i>
                            </span>
                        <div class="card-body p-0 content">
                            <h5>directory</h5>
                            <p class="para text-muted mb-0">It is a long established fact that a reader.</p>
                        </div>
                        <span class="big-icon text-center">
                                <i class="uil uil-users-alt"></i>
                            </span>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-4 mt-4 pt-2">
                    <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-file-alt"></i>
                            </span>
                        <div class="card-body p-0 content">
                            <h5>community</h5>
                            <p class="para text-muted mb-0">It is a long established fact that a reader.</p>
                        </div>
                        <span class="big-icon text-center">
                                <i class="uil uil-file-alt"></i>
                            </span>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-4 mt-4 pt-2">
                    <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                            <span class="h1 icon2 text-primary">
                                <i class="uil uil-search"></i>
                            </span>
                        <div class="card-body p-0 content">
                            <h5>support & help</h5>
                            <p class="para text-muted mb-0">It is a long established fact that a reader.</p>
                        </div>
                        <span class="big-icon text-center">
                                <i class="uil uil-search"></i>
                            </span>
                    </div>
                </div><!--end col-->


            </div><!--end row-->
        </div><!--end container-->

        <!-- About Start -->

    </section>
    <section class="section">
        <div class="container">
            <div class="row mt-lg-4 align-items-center">
                <div class="col-lg-4 col-md-12 text-center text-lg-start">
                    <div class="section-title mb-4 mb-lg-0 pb-2 pb-lg-0">
                        <span>Get Access To GstCentral Pro Today!</span>
                        <h1 class="title mb-4"> Now at exciting <br>
                            Introductory prices </h1>
                        <p class="text-muted para-desc mx-auto mb-0"><span class="text-primary fw-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                        <a href="https://1.envato.market/4n73n" target="_blank" class="btn btn-primary mt-4">Buy Now <span class="badge rounded-pill bg-danger ms-2">v3.0.0</span></a>
                    </div>
                </div><!--end col-->

                <div class="col-lg-8 col-md-12">
                    <div class="ms-lg-5">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-12 mt-4 pt-4 px-md-0">
                                <div class="card pricing-rates starter-plan shadow rounded border-0">
                                    <div class="ribbon ribbon-right ribbon-warning overflow-hidden"><span class="text-center d-block shadow small h6">Best</span></div>
                                    <div class="card-body py-5">
                                        <h6 class="title fw-bold text-uppercase text-primary mb-4"><s>₹4999</s> Save 50%</h6>
                                        <div class="d-flex mb-4">
                                            <span class="h4 mb-0 mt-2">₹</span>
                                            <span class="price h1 mb-0">3600</span>
                                            <span class="h4 align-self-end mb-1">/Year</span>
                                        </div>


                                        <ul class="list-unstyled mb-0 ps-0">
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>full access to gst library</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>advanced search with filters & tags</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>tools & resources</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>daily briefings & analysis</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>gst live feed</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>unlimited collections</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>ad-free experience</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>free monthly Newsletter</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>premium Support & help</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>access to premium courses </li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>anytime, anywhere, any device.</li>

                                        </ul>
                                        <a href="javascript:void(0)" class="btn btn-soft-success mt-4">subscribe now!</a>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-md-6 col-12 mt-4 pt-2 pt-md-4 px-md-0">
                                <div class="card pricing-rates bg-soft-primary shadow rounded border-0">
                                    <div class="card-body py-5">
                                        <h6 class="title fw-bold text-uppercase text-primary mb-4"><s>₹799</s> Save 50%</h6>
                                        <div class="d-flex mb-4">
                                            <span class="h4 mb-0 mt-2">₹</span>
                                            <span class="price h1 mb-0">399</span>
                                            <span class="h4 align-self-end mb-1">/mo</span>
                                        </div>

                                        <ul class="list-unstyled mb-0 ps-0">
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>full access to gst library</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>advanced search with filters & tags</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>tools & resources</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>daily briefings & analysis</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>gst live feed</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>unlimited collections</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>ad-free experience</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>free monthly Newsletter</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>premium Support & help</li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>access to premium courses </li>
                                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>anytime, anywhere, any device.</li>
                                        </ul>
                                        <a href="javascript:void(0)" class="btn btn-soft-success mt-4">subscribe now!</a>
                                    </div>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->




    </section>

    <section class="section">


        <div class="container mt-100 mt-60">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="me-lg-5">
                        <img src="images/illustrator/big-launch.svg" class="img-fluid" alt="">
                    </div>
                </div><!--end col-->

                <div class="col-md-6 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="section-title">
                        <h4 class="title mb-4">We help you increase <br> sales by improving SEO</h4>
                        <p class="text-muted">You can combine all the Landrick templates into a single one, you can take a component from the Application theme and use it in the Website.</p>
                        <ul class="list-unstyled text-muted">
                            <li class="mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>Digital Marketing Solutions for Tomorrow</li>
                            <li class="mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>Our Talented &amp; Experienced Marketing Agency</li>
                            <li class="mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>Create your own skin to match your brand</li>
                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">See more solutions <i class="uil uil-angle-right-b"></i></a>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

        <div class="container mt-100 mt-60">
            <div class="row align-items-center">
                <div class="col-md-6 order-1 order-md-2">
                    <div class="ms-lg-5">
                        <img src="images/illustrator/maintenance.svg" class="img-fluid" alt="">
                    </div>
                </div><!--end col-->

                <div class="col-md-6 order-2 order-md-1 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="section-title">
                        <h4 class="title mb-4">Advantages of SEO &amp; <br> Marketing strategy</h4>
                        <p class="text-muted">You can combine all the Landrick templates into a single one, you can take a component from the Application theme and use it in the Website.</p>
                        <p class="text-muted">Using Landrick to build your site means never worrying about designing another page or cross browser compatibility. Our ever-growing library of components and pre-designed layouts will make your life easier.</p>
                        <div class="mt-4">
                            <a href="javascript:void(0)" class="btn btn-pills btn-soft-primary">Know more</a>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->


    </section>

@endsection
