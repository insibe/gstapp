
    <meta charset="utf-8" />
    <title>GstCentral | All-in-one GST Cloud Platform</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="India's most-trusted Cloud Platform For GST Updates, Guides,Tools & Resources in Regional Languages." />
    <meta name="keywords" content="Saas, Software, multi-uses, HTML, Clean, Modern" />
    <meta name="author" content="GstCentral" />
    <meta name="email" content="info@gstcentral.com" />
    <meta name="website" content="gstcentral.com" />
    <meta name="Version" content="v0.1" />
    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/images/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/images/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/images/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/images/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/images/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/images/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/images/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/images/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon-16x16.png" sizes="16x16') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon-32x32.png" sizes="32x32') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon-96x96.png" sizes="96x96') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/android-chrome-192x192.png') }}" sizes="192x192">
    <meta name="msapplication-square70x70logo" content="{{ asset('assets/images/smalltile.png') }}" />
    <meta name="msapplication-square150x150logo" content="{{ asset('assets/images/mediumtile.png') }}" />
    <meta name="msapplication-wide310x150logo" content="{{ asset('assets/images/widetile.png') }}" />
    <meta name="msapplication-square310x310logo" content="{{ asset('assets/images/largetile.png') }}" />
    <!-- Bootstrap -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="{{ asset('assets/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.6/css/line.css">
    <!-- Slider -->
    <link rel="stylesheet" href="{{ asset('assets/css/tiny-slider.css') }}"/>
    <!-- Main Css -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" id="theme-opt" />
    <link href="{{ asset('assets/css/colors/default.css')}}" rel="stylesheet" id="color-opt">

