@extends('layouts.inner')

@section('content')
    <section class=" section bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <h4 class="title"> Hello! <br> How can we help you? </h4>

                        <div class="subcribe-form mt-4 pt-2">
                            <form>
                                <div class="mb-0">
                                    <input type="text" id="help" name="name" class="border bg-white rounded-pill" required="" placeholder="Search your questions or topic...">
                                    <button type="submit" class="btn btn-pills btn-primary">Search</button>
                                </div>
                            </form>
                        </div>


                    </div>
                </div><!--end col-->
            </div><!--end row-->
            <div class="row mt-4 pt-2">
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/bike.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Acts</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="{{ asset('assets/images/insurance/health.svg') }}" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Rules</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/term-life.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Forms</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/term-life.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Notifications</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/term-life.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Circulars</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/term-life.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Caselaws</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/term-life.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Press Release</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/family-health.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Orders</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/bike.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Removal of Difficulty Orders</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/investment.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Ordinance</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/investment.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Videos</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-2 col-md-4 col-6 mt-4 pt-2">
                    <div class="card explore-feature border-0 rounded text-center bg-white">
                        <div class="card-body">
                            <div class="icon rounded-circle shadow-lg d-inline-block">
                                <img src="images/insurance/investment.svg" class="avatar avatar-md-sm" alt="">
                            </div>
                            <div class="content mt-3">
                                <h6 class="mb-0"><a href="javascript:void(0)" class="title text-dark">Guides</a></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->




            </div>
        </div> <!--end container-->
    </section>


    <section class="section  pb-md-6 " id="featured">
        <div class="container">
            <div class="row mb-5 justify-content-center">
                <div class="col-12 col-md-6 col-lg-4 mb-4 ">
                    <h5 class="font-weight-bold">What's New?</h3>
                        <p class="text-black-50">Learn everything about GST for Business </p>
                        <ul class="list-unstyled text-muted">

                                <li><a href="#">Advisory for GSTR 9 and GSTR 9C by GSTN</a> </li>
                                <li><a href="#">Representation Seeking Extension Of Due Date For Furnishing GSTR 9C For 2019-20</a></li>
                                <li><a href="#">Applicability of Dynamic QR Code on B2C invoices & compliance.</a></li>
                                <li><a href="#">Advisory on Annual Return (GSTR-9)</a></li>
                                <li><a href="#">Various Important Due Dates released by ICMAI</a></li>


                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">Find Out More <i class="uil uil-angle-right-b"></i></a>

                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-4  border-left">

                    <h5 class="font-weight-bold">Gst Council Meeting Updates</h3>
                        <p class="text-black-50">Learn everything about GST for Business </p>
                        <ul class="list-unstyled text-muted">
                                <li><a href="#">42nd GST Council Meetings Updates <br> 05th October 2020 </a> </li>
                                <li><a href="#">41st GST Council Meeting  Updates <br> 27th August 2020  </a> </li>
                                <li><a href="#">40th GST Council Meetings Updates <br> 12th June 2020 </a> </li>
                                <li><a href="#">39th GST Council Meetings Updates <br> 05th October 2020 </a> </li>
                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">Find Out More <i class="uil uil-angle-right-b"></i></a>

                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-4  border-left ">
                    <h5 class="font-weight-bold">Notification / Circulars</h3>
                        <p class="text-black-50">Learn everything about GST for Business </p>
                        <ul class="list-unstyled text-muted">
                            <li><a href="#">Seeks to make amendment (2021) to CGST Rules, 2017</a> </li>
                            <li><a href="#">Commissioner of commercial Taxes Circular No. GST-14/2020-21</a></li>
                            <li><a href="#">Waiver from recording of UIN on the invoices for the months of April 2020 to March 2021.</a></li>
                            <li><a href="#">Seeks to extend the due dates for compliances and actions in respect of anti-profiteering measures under GST till 31.03.2021.</a></li>

                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">Find Out More <i class="uil uil-angle-right-b"></i></a>
                </div>


            </div> <!-- / .row -->
            <div class="row mt-5 justify-content-center">
                <div class="col-12 col-md-6 col-lg-4 mb-4 ">
                    <h5 class="font-weight-bold">What's New?</h3>
                        <p class="text-black-50">Learn everything about GST for Business </p>
                        <ul class="list-unstyled text-muted">

                            <li><a href="#">Advisory for GSTR 9 and GSTR 9C by GSTN</a> </li>
                            <li><a href="#">Representation Seeking Extension Of Due Date For Furnishing GSTR 9C For 2019-20</a></li>
                            <li><a href="#">Applicability of Dynamic QR Code on B2C invoices & compliance.</a></li>
                            <li><a href="#">Advisory on Annual Return (GSTR-9)</a></li>
                            <li><a href="#">Various Important Due Dates released by ICMAI</a></li>


                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">Find Out More <i class="uil uil-angle-right-b"></i></a>

                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-4  border-left">

                    <h5 class="font-weight-bold">Gst Council Meeting Updates</h3>
                        <p class="text-black-50">Learn everything about GST for Business </p>
                        <ul class="list-unstyled text-muted">
                            <li><a href="#">42nd GST Council Meetings Updates <br> 05th October 2020 </a> </li>
                            <li><a href="#">41st GST Council Meeting  Updates <br> 27th August 2020  </a> </li>
                            <li><a href="#">40th GST Council Meetings Updates <br> 12th June 2020 </a> </li>
                            <li><a href="#">39th GST Council Meetings Updates <br> 05th October 2020 </a> </li>
                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">Find Out More <i class="uil uil-angle-right-b"></i></a>

                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-4  border-left ">
                    <h5 class="font-weight-bold">Notification / Circulars</h3>
                        <p class="text-black-50">Learn everything about GST for Business </p>
                        <ul class="list-unstyled text-muted">
                            <li><a href="#">Seeks to make amendment (2021) to CGST Rules, 2017</a> </li>
                            <li><a href="#">Commissioner of commercial Taxes Circular No. GST-14/2020-21</a></li>
                            <li><a href="#">Waiver from recording of UIN on the invoices for the months of April 2020 to March 2021.</a></li>
                            <li><a href="#">Seeks to extend the due dates for compliances and actions in respect of anti-profiteering measures under GST till 31.03.2021.</a></li>

                        </ul>
                        <a href="javascript:void(0)" class="mt-3 h6 text-primary">Find Out More <i class="uil uil-angle-right-b"></i></a>
                </div>


            </div> <!-- / .row -->
        </div> <!-- / .container -->

    </section>
@endsection
