<?php

use Illuminate\Support\Facades\Route;
use App\Models\Category;
use App\Models\Act;
use App\Models\Section;
use Illuminate\Support\Facades\Artisan;
use Venturecraft\Revisionable\Revision;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/gst/{slug}', function ($slug) {


    $act = \App\Models\Act::where('slug', $slug )
            ->with('chapters')
            ->with('sections')
            ->firstOrFail();

    return view('test',compact('act'));
});

Route::get('/acts/', function () {

    $acts = Act::where('status','0')->orderby("order")->get();
    return view('sections',compact('acts'));
});

Route::get('/acts/{slug}', function ($slug) {

    $act = \App\Models\Act::where('slug', $slug )
        ->firstOrFail();
    $data = \App\Models\Chapter::with('acts')->where('act_id',$act->id)->get();
   return $data;
});


Route::get('/calendar', function () {

    $calendars = \App\Models\Calendar::orderby("order")->get();

    return view('calendars',compact('calendars'));
});


Route::get('/sample', function () {

    $acts = Act::with('chapters')->get();

    return $acts;
});

Route::get('/ss', function () {

    $acts = Act::with('chapters')->get();


    return $acts;
//    return view('test',compact('acts'));
});


//Route::get('/', function () {
//    $getGstLaws =  Category::withDepth()->having('depth', '=',0)->where('status','0')->get();
//
//
//    return view('home',compact('getGstLaws'));
//});





Route::get('/gst/{slug}', function ($slug) {

    $category = Category::where('slug', $slug )
        ->firstOrFail();;

    $acts = $category->acts()->orderBy('wef','desc')->get();

    return view('acts',compact('category','acts'));
});

Route::get('category/{id}/acts/', function ($id) {

    $category = Category::where('id', $id )
        ->firstOrFail();

    $getacts = $category->acts()->orderBy('wef','desc')->get();
    return view('acts',compact('getacts'));
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function () {
    // Admin Dashboard
    Route::get('/dashboard', function () {
        return view('dashboard.index');
    });

    Route::get('/user/profile', function () {
        return view('user.profile');
    });
    Route::get('/user/profile/basic-info', function () {
        return view('user.basic-info');
    });

    Route::resource('dashboard/users', \App\Http\Controllers\UserController::class);
    Route::resource('dashboard/categories', \App\Http\Controllers\CategoryController::class);
    Route::resource('dashboard/feeds', \App\Http\Controllers\FeedController::class);
    Route::get('dashboard/feeds/{id}/fetch', [\App\Http\Controllers\FeedController::class, 'fetch']);
    Route::resource('dashboard/posts', \App\Http\Controllers\PostController::class);
    Route::resource('dashboard/acts', \App\Http\Controllers\ActController::class);
    Route::resource('dashboard/sections', \App\Http\Controllers\SectionController::class);
    Route::resource('dashboard/sections/{id}/subsections', \App\Http\Controllers\SubSectionController::class);
    Route::resource('dashboard/chapters', \App\Http\Controllers\ChapterController::class);
    Route::resource('dashboard/calendars', \App\Http\Controllers\CalendarController::class);


    Route::get('dashboard/settings/system-log', function () {

        $systemLogs = \Venturecraft\Revisionable\Revision::paginate('30');


        return view('dashboard.settings.system-log',compact('systemLogs'));
    });
//
//    Route::resource('dashboard/posts', \App\Http\Controllers\PostController::class);
//    Route::resource('dashboard/businesses', \App\Http\Controllers\BusinessController::class);
//    Route::resource('dashboard/feeds', \App\Http\Controllers\FeedController::class);
//    Route::get('dashboard/feeds/{id}/fetch', [\App\Http\Controllers\FeedController::class, 'fetch']);
//    Route::resource('dashboard/categories', \App\Http\Controllers\CategoryController::class);
});


Route::get('/getActsLaws', function () {
    $getActsLaws = Category::defaultOrder()->get();
    return $getActsLaws;
});

Route::get('/getSections', function () {
    $getSections = Section::with('subsections')->get();

    return view('home');

});


Route::get('/settings', function () {

    $revisions_grouped = \Venturecraft\Revisionable\Revision::latest('id')->limit(500)->get()
        ->groupBy(function($revision) {
            return $revision->created_at->format('d.m.Y');
        });
    foreach($revisions_grouped as $date => $revisions) {
        echo '<strong>'.$date.'</strong><ul>';
        foreach($revisions as $revision) {
            echo '<li>'.$revision->created_at->format('h:i a').': modified <strong>'.$revision->key.'</strong> on '.
                class_basename($revision->revisionable_type).' '.$revision->revisionable_id.'</li>';
        }
        echo '</ul><hr />';
    }
    return view('dashboard.settings.index',compact('revisions_grouped'));

});


//Clear Cache facade value:
Route::get('/clear-cache', function() {

    return '<h1>Cache facade value cleared</h1>';
});



//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    return '<h1>Cache facade value cleared</h1>';
});


Route::get('/migrate', function(){
    Artisan::call('migrate');
    return '<h1>MIGRATE</h1>';
});


Route::get('/', function () {
    return view('welcome');
});
Route::get('/company/about-us', function () {
    return view('about');
});

Route::get('/company/investor-relations', function () {
    return view('investor-relations');
});

Route::get('/company/media-kit', function () {
    return view('media-kit');
});

Route::get('/company/careers', function () {
    return view('careers');
});

Route::get('/company/our-team', function () {
    return view('our-team');
});


Route::get('/legal/terms-of-service', function () {
    return view('terms-of-service');
});

Route::get('/legal/cookie-policy', function () {
    return view('cookie-policy');
});

Route::get('/legal/privacy-policy', function () {
    return view('privacy-policy');
});

Route::get('/legal/editorial-policy', function () {
    return view('editorial-policy');
});



Route::get('/pro', function () {
    return view('pro');
});

Route::get('/consult', function () {
    return view('consult.index');
});

Route::get('/community', function () {
    return view('community.index');
});



Route::get('/learn', function () {
    return view('learn.index');
});

Route::get('/resources', function () {
    return view('resources.index');
});

Route::get('/account', function () {
    return view('account.index');
})->middleware(['auth'])->name('dashboard');


Route::get('/verification', function () {
    return view('account.verification');
})->middleware(['auth'])->name('verification');


Route::get('/generate-pdf', function () {
    return view('dashboard.index');
});
