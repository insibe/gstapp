<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Section;
use App\Models\SubSection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SubSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $subsections =  SubSection::orderby("order")->paginate(15);

        $data = [
            'page_title' => 'Manage Sub Section'
        ];

        return view('dashboard.subsections.index',compact('subsections'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($sectionid)
    {


        $categories =   Category::pluck('title','id');

        $data = [
            'subsection' => null,
            'formMethod' => 'POST',
            'mode'=> 'create',
            'url' => 'dashboard/sections/'.$sectionid.'/subsections/',
            'page_title' => 'Add a New Sub Section'
        ];

        return view('dashboard.subsections.edit',compact('categories'),$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {




        try {

            $subsection = new SubSection();
            $subsection->public_id              = '1';
            $subsection->locale                 = $request->get('locale');
            $subsection->section_id             = $request->get('section_id');
            $subsection->parent_id              = $request->get('parent_id') ?? null ;
            $subsection->revision_id            = $request->get('revision_id') ?? null ;
            $subsection->title                  = $request->get('title');
            $subsection->content                = $request->get('content');
            $subsection->content_formatted      = $request->get('content_formatted');
            $subsection->wef                    = $request->get('wef');
            $subsection->order                  = $request->get('order');
            $subsection->status                 = $request->get('status');
            $subsection->save();





            Alert::success('Success', 'Subsection Created Successfully');
            return redirect('dashboard/sections/'.$subsection->section_id.'/')->with('success', 'Section Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubSection  $subSection
     * @return \Illuminate\Http\Response
     */
    public function show(SubSection $subSection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubSection  $subSection
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {

        $current_params = \Route::current()->parameters();

        $subsectionID =  $current_params['subsection'];


        $subsection = SubSection::where('id', $subsectionID)->firstOrFail();


        $categories =   Category::pluck('title','id');

        $data = [
            'subsection' => $subsection,
            'formMethod' => 'PUT',
            'mode'=> 'edit',
            'url' => 'dashboard/sections/'.$subsection->section_id.'/subsections/'.$subsection->id,
            'page_title' => ' Edit # '.$subsection->title
        ];



        return view('dashboard.subsections.edit',compact('categories'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubSection  $subSection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $current_params = \Route::current()->parameters();

        $subsectionID =  $current_params['subsection'];


        try {
            $subsection = SubSection::findOrFail($subsectionID);

            $subsection->public_id              = '1';
            $subsection->locale                 = $request->get('locale');
            $subsection->section_id             = $request->get('section_id');
            $subsection->revision_id            = $request->get('revision_id') ?? 0 ;
            $subsection->title                  = $request->get('title');
            $subsection->content                = $request->get('content');
            $subsection->content_formatted      = $request->get('content_formatted');
            $subsection->wef                    = $request->get('wef');
            $subsection->order                  = $request->get('order');
            $subsection->status                 = $request->get('status');
            $subsection->save();

            Alert::success('Success', 'Subsection Updated Successfully');
            return redirect('dashboard/sections/'.$subsection->section_id.'/')->with('success', 'Section Created Successfully!');
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubSection  $subSection
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sectionID = $id;

        $current_params = \Route::current()->parameters();

        $subsectionID =  $current_params['subsection'];

        $subsection = SubSection::find($subsectionID);



        if ($subsection != null) {
            $subsection->delete();
            Alert::alert('Deleted', 'SubSection Deleted Successfully');
            return redirect('dashboard/sections/'.$sectionID.'/')->with('Deleted', 'SubSection Deleted Successfully!');
        }
//
//        return redirect('dashboard/sections/')->with('Deleted', 'Sub Section Deleted Successfully!');
    }
}
