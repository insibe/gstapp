<?php

namespace App\Http\Controllers;

use App\Models\Act;
use App\Models\Category;
use App\Models\Chapter;
use App\Models\Section;
use App\Models\SubSection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections =  Section::with('chapters')
                            ->orderBy('chapter_id')
                            ->orderBy('order')
                            ->paginate(15);




        $data = [
            'page_title' => 'Manage Sections'
        ];

        return view('dashboard.sections.index',compact('sections'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {



        $chapters = Chapter::with('acts')->orderBy('order')->get()->map(function($chapters )
        {
            return [  $chapters->title.' - ' .$chapters->acts->title  => $chapters->id

            ];

        })->collapse();

        $chapters = $chapters->flip()->all();



        $data = [
            'section' => null,
            'formMethod' => 'POST',
            'mode' => 'create',
            'url' => 'dashboard/sections',
            'page_title' => 'Add a New Section'
        ];

        return view('dashboard.sections.edit',compact('chapters'),$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {




        try {

            $section = new Section();
            $section->public_id              = '1';
            $section->locale                 = $request->get('locale');
            $section->section_no             = $request->get('section_no');
            $section->chapter_id             = $request->get('chapter_id');
            $section->section_heading        = $request->get('section_heading');
            $section->section_header         = $request->get('section_header');
            $section->section_footer         = $request->get('section_footer');
            $section->wef                    = $request->get('wef');
            $section->order                  = $request->get('order');
            $section->status                 = $request->get('status');
            $section->save();

            Alert::success('Success', 'Section Created Successfully');
            return redirect('dashboard/sections/'.$section->id.'/')->with('success', 'Section Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $section = Section::where('id', $id)->firstOrFail();

        $subsections = SubSection::where('section_id', $id)->get();



        $data = [
            'section' => $section,
            'formMethod' => 'PUT',
            'url' => 'dashboard/sections/'.$section->id.'/subsections/'.$id,
            'page_title' => ' Manage # '.$section->section_no
        ];
        return view('dashboard.sections.manage',compact('subsections'),$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section = Section::where('id', $id)->firstOrFail();


        $chapters = Chapter::with('acts')->orderBy('order')->get()->map(function($chapters )
        {
            return [  $chapters->title.' - ' .$chapters->acts->title  => $chapters->id

            ];

        })->collapse();

        $chapters = $chapters->flip()->all();



        $data = [
            'section' => $section,
            'formMethod' => 'PUT',
            'mode' => 'edit',
            'url' => 'dashboard/sections/'.$id,
            'page_title' => ' Edit # '.$section->title
        ];



        return view('dashboard.sections.edit',compact('chapters'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {


        try {
            $section = Section::findOrFail($id);
            $section->public_id              = '1';
            $section->locale                 = $request->get('locale');
            $section->section_no             = $request->get('section_no');
            $section->chapter_id                 = $request->get('chapter_id');
            $section->section_heading        = $request->get('section_heading');
            $section->section_header         = $request->get('section_header');
            $section->section_footer         = $request->get('section_footer');
            $section->wef                    = $request->get('wef');
            $section->order                  = $request->get('order');
            $section->status                 = $request->get('status');
            $section->save();



            Alert::success('Success', 'Section Updated Successfully');
            return redirect('dashboard/sections/'.$section->id.'/')->with('success', 'Section Updated Successfully!');
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::find($id);
        $section->delete();
        Alert::alert('Deleted', 'Section Deleted Successfully');
        return redirect('dashboard/sections/')->with('Deleted', 'Section Deleted Successfully!');
    }
}
