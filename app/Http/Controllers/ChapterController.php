<?php

namespace App\Http\Controllers;

use App\Models\Act;
use App\Models\Chapter;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chapters =  Chapter::paginate(15);
        $data = [
            'page_title' => 'Manage Chapters'
        ];

        return view('dashboard.chapters.index',compact('chapters'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $acts =    Act::pluck('title','id')->all();
        $data = [
            'chapter' => null,
            'formMethod' => 'POST',
            'mode' => 'CREATE',
            'url' => 'dashboard/chapters',
            'page_title' => 'Add a New Chapter'
        ];

        return view('dashboard.chapters.edit',compact('acts'),$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $chapter = new Chapter();
            $chapter->locale                 = 'en';
            $chapter->title                  = $request->get('title');
            $chapter->subtitle               = $request->get('subtitle');
            $chapter->act_id                 = $request->get('act_id');
            $chapter->order                  = $request->get('order');
            $chapter->status                 = $request->get('status');
            $chapter->save();

            Alert::success('Success', 'Chapter Added Successfully');
            return redirect('dashboard/chapters/'.$chapter->id.'/edit')->with('success', 'Chapter Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function show(Chapter $chapter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chapter = Chapter::where('id', $id)->firstOrFail();

        $acts    = Act::pluck('title','id')->all();

        $data = [
            'chapter' => $chapter,
            'formMethod' => 'PUT',
            'mode' => 'EDIT',
            'url' => 'dashboard/chapters/'.$id,
            'page_title' => ' Edit # '.$chapter->title
        ];



        return view('dashboard.chapters.edit',compact('acts'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $chapter = Chapter::findOrFail($id);

            $chapter->locale                 = 'en';
            $chapter->title                  = $request->get('title');
            $chapter->subtitle               = $request->get('subtitle');
            $chapter->act_id                 = $request->get('act_id');
            $chapter->order                  = $request->get('order');
            $chapter->status                 = $request->get('status');
            $chapter->save();



            Alert::success('Success', 'Chapter Updated Successfully');
            return redirect('dashboard/chapters/'.$chapter->id.'/edit')->with('success', 'Chapter Updated Successfully!');
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chapter $chapter)
    {
        //
    }
}
