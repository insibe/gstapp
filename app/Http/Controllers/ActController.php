<?php

namespace App\Http\Controllers;

use App\Models\Act;
use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\Category;
use Kalnoy\Nestedset\Collection;
use RealRashid\SweetAlert\Facades\Alert;

class ActController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $acts =  Act::paginate(15);
        $data = [
            'page_title' => 'Manage Act'
        ];

        return view('dashboard.acts.index',compact('acts'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories =   $this->getCategoryOptions();

        $data = [
            'act' => null,
            'formMethod' => 'POST',
            'mode' => 'create',
            'url' => 'dashboard/acts',
            'page_title' => 'Add a New Act'
        ];

        return view('dashboard.acts.edit',compact('categories'),$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $act = new Act();
            $act->public_id           = '1';
            $act->title               = $request->get('title');
            $act->locale              = 'en';
            $act->description         = $request->get('description');
            $act->wef                 = $request->get('wef');
            $act->order               = $request->get('order');
            $act->status              = $request->get('status');
            $act->save();

            $act->categories()->sync($request->input('categories'));

            return redirect('dashboard/acts/'.$act->id.'/edit')->with('success', 'Acts Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Act  $acts
     * @return \Illuminate\Http\Response
     */
    public function show(Act $acts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Act  $acts
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $act = Act::where('id', $id)->firstOrFail();
        $categories =   $this->getCategoryOptions();

        $data = [
            'act' => $act,
            'formMethod' => 'PUT',
            'mode' => 'edit',
            'url' => 'dashboard/acts/'.$id,
            'page_title' => ' Edit # '.$act->title
        ];



        return view('dashboard.acts.edit',compact('categories'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Act  $acts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {

            $act = Act::findOrFail($id);
            $act->public_id           = '1';
            $act->title               = $request->get('title');
            $act->locale              = 'en';
            $act->description         = $request->get('description');
            $act->wef                 = $request->get('wef');
            $act->order               = $request->get('order');
            $act->status              = $request->get('status');
            $act->save();
            $act->categories()->sync($request->input('categories'));

            Alert::success('Success', 'Act Updated Successfully');
            return redirect('dashboard/acts/'.$act->id.'/edit')->with('success', 'Act Updated Successfully!');
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Act  $acts
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $act = Act::find($id);
        $act->delete();
        Alert::alert('Deleted', 'Act Deleted Successfully');
        return redirect('dashboard/acts/')->with('Deleted', 'Act Deleted Successfully!');
    }

    protected function makeOptions(Collection $items)
    {
        $options = [' ' => 'None' ];
        foreach ($items as $item)
        {
            $options[$item->getKey()] = str_repeat('- ', $item->depth + 2).''.$item->title;
        }
        return $options;
    }

    /**
     * @param Category $except
     *
     * @return CategoriesController
     */
    protected function getCategoryOptions($except = null)
    {
//        /** @var \Kalnoy\Nestedset\QueryBuilder $query */
        $query = Category::select('id', 'title')->withDepth();
        if ($except)
        {
            $query->whereNotDescendantOf($except)->where('id', '<>', $except->id);
        }
        return $this->makeOptions($query->get());
    }
}
