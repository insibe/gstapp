<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use \Venturecraft\Revisionable\RevisionableTrait;

class Section extends Model
{
    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;

    use RevisionableTrait;
    use HasSlug;
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    use HasFactory;


    public function subsections(){
        return $this->hasMany(SubSection::class);
    }


    /**
     * Get the post that owns the comment.
     */
    public function acts()
    {
        return $this->belongsTo(Act::class);
    }


    public function chapters(){

        return $this->belongsTo(Chapter::class,'chapter_id');
    }

    public function getIdNameAttribute($value)
    {
        return $this->id.' - '.$this->id;
    }

}
