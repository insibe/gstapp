<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use \Venturecraft\Revisionable\RevisionableTrait;
class SubSection extends Model
{
    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;

    use RevisionableTrait;



    use HasFactory;

    use HasSlug;
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function section(){
        return $this->belongsTo(Section::class);
    }



    public function childs()
    {
        return $this->hasMany(SubSection::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(SubSection::class, 'parent_id');
    }
}
