<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use \Venturecraft\Revisionable\RevisionableTrait;

class Category extends Model
{
    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $fillable = ['title','slug','status'];
    use NodeTrait;
    use HasSlug;
    use HasFactory;
    use RevisionableTrait;

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getCategoryAttribute()
    {
        return $this->category()->pluck('id')->toArray();
    }

    public function posts()
    {
        return $this->belongsToMany(Category::Class, 'post_categories');
    }


    public function acts()
    {
        return $this->belongsToMany(Act::Class, 'act_categories');
    }

}
