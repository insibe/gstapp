<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use GoogleTranslate;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;


class Feed extends Model
{
    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    use SoftDeletes;
    use HasSlug;

    use HasFactory;

    protected $dates = ['deleted_at'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }


    public  function getStatus(){

        $status = $this->status;
        switch ($status) {
            case "0":
                echo '<span class="badge badge-success">ACTIVE</span>';
                break;
            case "1":
                echo '<span class="badge badge-secondary">INACTIVE</span>';
                break;
        }


    }


    public  function getType(){

       $type = $this->type;
        switch ($type) {
            case "wapi":
                echo '<em class="icon ni ni-wordpress"></em>';
                break;
            case "rss":
                echo '<em class="icon ni ni-rss"></em>';
                break;
        }
    }

    public function post()
    {
        return $this->belongsTo(Feed::class);
    }

    public  function getTargetLanguage(){

        $target = $this->target_lang;
        switch ($target) {
            case "en":
                echo '<span class="badge badge-outline-primary">ENGLISH</span>';
                break;
            case "ml":
                echo '<span class="badge badge-outline-primary">MALAYALAYAM</span>';
                break;
        }
    }

    public  function getSourceLanguage(){

        $source = $this->source_lang;
        switch ($source) {
            case "en":
                echo '<span class="badge badge-outline-primary">ENGLISH</span>';
                break;
            case "ml":
                echo '<span class="badge badge-outline-primary">MALAYALAYAM</span>';
                break;
        }
    }




}
