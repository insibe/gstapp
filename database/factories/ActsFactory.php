<?php

namespace Database\Factories;

use App\Models\Act;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class ActsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Act::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'public_id' =>  Act::count() + 1,// password
            'locale' => 'en',
            'title' => $this->faker->company(),
            'slug' => $this->faker->company(),
            'description' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'order' =>  $this->faker->numberBetween($min = 0, $max = 100), // password
            'status' => $this->faker->randomElement(['0' ,'1']),
            'wef' =>  $this->faker->date,
        ];
    }
}
