<?php

namespace Database\Factories;

use App\Models\Act;
use App\Models\Section;
use Illuminate\Database\Eloquent\Factories\Factory;

class SectionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Section::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $number = 1;

        return [
            'public_id' =>  Section::count() + 1,// password
            'locale' => 'en',
            'section_no' => 'Section - '.$number++ ,
            'slug' => $this->faker->company(),
            'section_heading' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'section_header' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'section_footer' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'order' =>  $this->faker->numberBetween($min = 0, $max = 100), // password
            'status' => $this->faker->randomElement(['0' ,'1']),
            'wef' =>  $this->faker->date,
        ];
    }
}
