<?php

namespace Database\Factories;

use App\Models\Act;
use App\Models\Section;
use App\Models\SubSection;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubSectionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubSection::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'public_id' =>  Section::count() + 1,// password
            'section_id' => $this->faker->randomElement(Section::pluck('id')->toArray()),
            'revision_id' =>  '1',// password
            'locale' => 'en',
            'title' => '2(a)',
            'slug' => $this->faker->company(),
            'content' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'content_formatted' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'order' =>  $this->faker->numberBetween($min = 0, $max = 100), // password
            'status' => $this->faker->randomElement(['0' ,'1']),
            'wef' =>  $this->faker->date,
        ];
    }
}
