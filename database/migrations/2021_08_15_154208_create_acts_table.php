<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acts', function (Blueprint $table) {

            $table->id();
            $table->unsignedInteger('public_id')->index();
            $table->string('locale');
            $table->string('title');
            $table->string('slug')->unique();
            $table->string('description')->nullable();
            $table->date('wef');
            $table->bigInteger('order')->nullable();
            $table->enum('status', ['0', '1']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acts');
    }
}
