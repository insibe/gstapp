<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_sections', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('public_id')->index();
            $table->foreignId('section_id')->unsigned();
            $table->unsignedInteger('revision_id')->index();
            $table->unsignedInteger('parent_id')->unsigned()->nullable();
            $table->string('locale');
            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('content')->nullable();;
            $table->longText('content_formatted')->nullable();;
            $table->date('wef')->nullable();;
            $table->bigInteger('order')->nullable();
            $table->enum('status', ['0', '1']);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_sections');
    }
}
