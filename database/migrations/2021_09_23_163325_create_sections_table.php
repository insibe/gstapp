<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('public_id')->index();
            $table->string('locale');
            $table->string('section_no');
            $table->string('slug')->unique();
            $table->longText('section_heading')->nullable();;
            $table->longText('section_header')->nullable();
            $table->longText('section_footer')->nullable();
            $table->date('wef')->nullable();;
            $table->bigInteger('order')->nullable();
            $table->enum('status', ['0', '1']);
            $table->softDeletes();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
